package org.example.dao;

import org.example.domain.User;

public interface UserDao {
    void add(User user);

    void show();
}
