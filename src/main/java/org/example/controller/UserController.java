package org.example.controller;

import org.example.domain.User;
import org.example.service.UserService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;


public class UserController {
    public static void main(String[] args) throws Exception {
        test1();
    }

    /**
     * 测试：初始化时加载类
     */
    private static void test02() {
        // 1、创建Spring的容器对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-spring.xml");
        // 2、从容器中获取userService对象
        UserService userService = applicationContext.getBean("userService", UserService.class);
        // 3、调用业务方法
        User user = new User("张三", 20);
        userService.add(user);
    }

    /**
     * 测试：调用时加载类
     */
    private static void test1() {
        // 1、创建Spring的容器对象
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("application-spring.xml"));
        // 2、从容器中获取userService对象
        UserService userService = beanFactory.getBean("userService", UserService.class);
        // 3、调用业务方法
        User user = new User("张三", 20);
        userService.add(user);
    }


}
