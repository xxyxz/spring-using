package org.example.service;

import org.example.domain.User;

public interface UserService {

    void add(User user);

    void show();

}
