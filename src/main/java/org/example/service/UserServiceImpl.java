package org.example.service;

import org.example.domain.User;
import org.example.dao.UserDao;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    private String msg;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserServiceImpl(){
        System.out.println("UserService was created");
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void add(User user) {
        System.out.println(msg);
        userDao.add(user);
    }

    @Override
    public void show() {
        this.userDao.show();
    }
}
